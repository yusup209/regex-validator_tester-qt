#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::testRegex(QString sourceStr, std::regex reg)
{
    try {
    if (sourceStr.isEmpty()){ ui->plainTextEdit_2->clear(); return; }
    std::string str = sourceStr.toStdString();
    std::sregex_iterator currentMatch (str.begin(), str.end(), reg);
    std::sregex_iterator lastMatch;
    QStringList strList;
    QString strJadi;

    while (currentMatch != lastMatch){
        std::smatch match = *currentMatch;
        strList.append(QString("%1\n").arg(QString::fromStdString(match.str())));

        strJadi += QString("%1\n").arg(QString::fromStdString(match.str()));
        currentMatch++;
    }

    ui->plainTextEdit_2->setPlainText(strJadi);
    } catch (std::regex_error &e) {
        QMessageBox::critical(this, "Error Bambank!", QString::fromStdString(e.what()));
        if (e.code() == std::regex_constants::error_brack){
            QMessageBox::critical(this, "Error Bambank!", "The code was error_brack");
        }
    }
}


void MainWindow::on_pushButton_clicked()
{
    //tombol validasi
    std::regex rgx(ui->lineEdit->text().toStdString());
    testRegex(ui->plainTextEdit->toPlainText(), rgx);
    //validasi email bambank
    //\w{1,20}[a-zA-Z]\d{0,5}?\.?\_?\%?\+?\-?@\w{2,20}[a-zA-Z]\d{0,5}?\.?\-?\.\w{2,3}[a-zA-Z]
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
//    std::regex rgx(ui->lineEdit->text().toStdString());
//    testRegex(arg1, rgx);
}
